package az.ruslan.unitech.unitechloginandregistration.controller;


import az.ruslan.unitech.unitechloginandregistration.dto.request.UserDto;
import az.ruslan.unitech.unitechloginandregistration.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("register")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/create")
    public ResponseEntity<?> registerUser(@RequestBody UserDto userDto) {
        boolean success = userService.registerUser(userDto);
        if (!success) {
            return ResponseEntity.badRequest().body("Pin already in use.");
        }
        return ResponseEntity.ok().body("User registered successfully.");
    }

    //for me to look users
    @GetMapping("/users")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok().body(userService.getUsers());
    }
}
