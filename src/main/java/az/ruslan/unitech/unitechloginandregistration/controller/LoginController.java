package az.ruslan.unitech.unitechloginandregistration.controller;

import az.ruslan.unitech.unitechloginandregistration.dto.request.LoginRequest;
import az.ruslan.unitech.unitechloginandregistration.dto.response.JwtResponse;
import az.ruslan.unitech.unitechloginandregistration.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class LoginController {


    private final LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {
        Map<String, String> login = loginService.login(loginRequest);
        return ResponseEntity.ok(new JwtResponse(login.get("jwt"),
                login.get("username")));
    }
}
