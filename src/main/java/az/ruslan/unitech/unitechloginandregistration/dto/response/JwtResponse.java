package az.ruslan.unitech.unitechloginandregistration.dto.response;

import lombok.Data;

@Data
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private String pin;

    public JwtResponse(String accessToken, String pin) {
        this.token = accessToken;
        this.pin = pin;
    }
}
