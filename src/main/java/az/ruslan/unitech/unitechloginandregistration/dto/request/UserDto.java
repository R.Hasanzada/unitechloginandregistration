package az.ruslan.unitech.unitechloginandregistration.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private String pin;
    private String name;
    private String surname;
    private String password;
    private boolean active;
    private LocalDateTime createdDate;
}
