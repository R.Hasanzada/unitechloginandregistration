package az.ruslan.unitech.unitechloginandregistration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniTechLoginAndRegistrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniTechLoginAndRegistrationApplication.class, args);
    }

}
