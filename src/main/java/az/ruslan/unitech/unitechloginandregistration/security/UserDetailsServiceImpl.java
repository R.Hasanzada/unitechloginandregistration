package az.ruslan.unitech.unitechloginandregistration.security;


import az.ruslan.unitech.unitechloginandregistration.entity.User;
import az.ruslan.unitech.unitechloginandregistration.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByPin(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with pin: " + username));
        return UserDetailsImpl.build(user);
    }
}
