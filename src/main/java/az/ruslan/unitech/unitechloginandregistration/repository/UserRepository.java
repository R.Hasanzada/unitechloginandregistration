package az.ruslan.unitech.unitechloginandregistration.repository;

import az.ruslan.unitech.unitechloginandregistration.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByPin(String pin);
    Optional<User> findByPin(String pin);
}