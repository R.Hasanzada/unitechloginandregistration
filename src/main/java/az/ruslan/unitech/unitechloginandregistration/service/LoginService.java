package az.ruslan.unitech.unitechloginandregistration.service;

import az.ruslan.unitech.unitechloginandregistration.dto.request.LoginRequest;
import az.ruslan.unitech.unitechloginandregistration.security.JwtUtil;
import az.ruslan.unitech.unitechloginandregistration.security.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.hibernate.id.uuid.UuidGenerator;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;


    public Map<String, String> login(LoginRequest loginRequest){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getPin(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtil.generateToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        Map<String, String> response = new HashMap<>();
        response.put("jwt", jwt);
        response.put("username", userDetails.getUsername());
        return response;
    }
}
