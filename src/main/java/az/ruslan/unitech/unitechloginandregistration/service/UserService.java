package az.ruslan.unitech.unitechloginandregistration.service;

import az.ruslan.unitech.unitechloginandregistration.dto.request.UserDto;
import az.ruslan.unitech.unitechloginandregistration.entity.User;
import az.ruslan.unitech.unitechloginandregistration.mapper.UserMapper;
import az.ruslan.unitech.unitechloginandregistration.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public boolean registerUser(UserDto userDto) {
        if (userRepository.existsByPin(userDto.getPin())) {
            return false; // User with this pin already exists
        }
        User user = UserMapper.INSTANCE.toEntity(userDto);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setActive(true);
        userRepository.save(user);
        return true;
    }

    public List<UserDto> getUsers() {
        return userRepository.findAll().stream()
                .map(UserMapper.INSTANCE::toDto)
                .collect(Collectors.toList());
    }


}
