package az.ruslan.unitech.unitechloginandregistration.mapper;
import az.ruslan.unitech.unitechloginandregistration.dto.request.UserDto;
import az.ruslan.unitech.unitechloginandregistration.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto toDto(User user);
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    User toEntity(UserDto userDto);

}
